package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.SysRole;
import com.five.vo.RoleVo;

public interface RoleService {
    //查询所有角色
    DataGridView queryAllRole(RoleVo roleVo);
    //添加角色
    void addRole(RoleVo roleVo);
    //修改角色
    void updateRole(RoleVo roleVo);
    //根据id删除角色
    void deleteRole(Integer id);
    //根据id加载角色管理分配菜单
    DataGridView initRolePermissionTreeJson(Integer id);
    //保存角色和菜单的关系
    void saveRolePermission(RoleVo roleVo);
    //根据角色名名查询角色
    SysRole queryRoleByName(String name);
}
