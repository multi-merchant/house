package com.five.service;

import com.five.common.DataGridView;
import com.five.common.exception.MyException;
import com.five.domain.SysUser;
import com.five.vo.UserVo;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends UserDetailsService {
    //加载用户列表
    DataGridView queryAllUser(UserVo userVo);
    //添加员工
    void addStaff(UserVo userVo) throws MyException;
    //修改员工
    void updateStaff(UserVo userVo);
    //加载用户管理的分配角色的数据
    DataGridView queryStaffRole(String id);
    //保存用户和角色的关系
    void saveStaffRole(UserVo userVo);
    //重置密码
    void resetPwd(String id);
    //禁用顾客
    void banCustomer(String id, Integer available);
    //修改密码
    void updatePwd(String oldpassword, String newpassword) throws MyException;
    //查询个人资料
    DataGridView queryUser();
    //统计用户总数
    DataGridView countUser(SysUser sysUser);
    //用户注册
    void addCustomer(UserVo userVo) throws MyException;
    //修改个人信息
    void updateInfo(UserVo userVo);
    //导航栏可用员工
    List<SysUser> queryStaffByUse();
}
