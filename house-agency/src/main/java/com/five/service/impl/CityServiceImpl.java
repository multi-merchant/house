package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.domain.City;
import com.five.mapper.CityMapper;
import com.five.service.CityService;
import com.five.vo.CityVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityMapper cityMapper;

    //查询城市信息
    @Override
    public List<City> queryAllCity(CityVo cityVo){
        List<City> data = cityMapper.queryAllCity(cityVo);
        return data;
    }

    //查询城市
    public City queryCityByName(CityVo cityVo){
        City data = cityMapper.queryCity(cityVo);
        return data;
    }

    //导航栏城市查询
    public List<City> queryCityByUse(CityVo cityVo){
        List<City> data = cityMapper.queryCityByUse(cityVo);
        return data;
    }

    //添加城市信息
    @Override
    public DataGridView addCity(CityVo cityVo){
        DataGridView dataGridView = new DataGridView();
        if(!cityMapper.queryAllCity(cityVo).isEmpty()){
            dataGridView.setCode(500);
            dataGridView.setMsg("城市已存在！！");
        }else {
            cityVo.setAvailable(1);
            cityMapper.alter();
            int row =cityMapper.addCity(cityVo);
            if(row == 0){
                dataGridView.setCode(500);
                dataGridView.setMsg("添加失败，请重新添加！！");
            }else {
                dataGridView.setCode(200);
                dataGridView.setMsg("添加成功！！");
            }
        }
        return dataGridView;
    }

    //修改城市信息
    @Override
    public DataGridView updateCity(CityVo cityVo){
        DataGridView dataGridView = new DataGridView();
        if(cityMapper.queryCityByName(cityVo.getName()) != null){
            dataGridView.setCode(500);
            dataGridView.setMsg("城市已存在！！");
        }else {
            int row = cityMapper.updateCity(cityVo);
            if(row == 1){
                dataGridView.setCode(200);
                dataGridView.setMsg("修改成功！！");
            }else {
                dataGridView.setCode(500);
                dataGridView.setMsg("修改失败！！");
            }
        }
        return dataGridView;
    }

    //删除(恢复)城市信息
    @Override
    public DataGridView deleteCity(CityVo cityVo){
        DataGridView dataGridView = new DataGridView();
        int row = cityMapper.deleteCity(cityVo);
        if(row == 1){
            dataGridView.setMsg("设置成功！！");
            dataGridView.setCode(200);
        }else{
            dataGridView.setCode(500);
            dataGridView.setMsg("设置失败！！");
        }
        return dataGridView;
    }

}
