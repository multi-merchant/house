package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.City;
import com.five.domain.Decoration;
import com.five.vo.CityVo;
import com.five.vo.DecorationVo;

import java.util.List;

public interface DecorationService {

    //查询装修风格
    List<Decoration> queryAllDecoration(DecorationVo decorationVo);

    //导航栏装修风格查询
    List<Decoration> queryDecorationByUse(DecorationVo decorationVo);

    //添加装修风格
    DataGridView addDecoration(DecorationVo decorationVo);

    //修改装修风格信息
    DataGridView updateDecoration(DecorationVo decorationVo);

    //删除(恢复)装修风格信息
    DataGridView deleteDecoration(DecorationVo decorationVo);
}
