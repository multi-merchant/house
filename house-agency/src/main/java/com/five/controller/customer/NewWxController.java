package com.five.controller.customer;

import com.five.common.file.AppFileUtils;
import com.five.domain.News;
import com.five.service.NewsService;
import com.five.vo.NewsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/user/new")
public class NewWxController {
    @Autowired
    NewsService newsService;

    @RequestMapping("/allNews")
    public List<News> listNews(NewsVo newsVo){
        newsVo.setAvailable(1);
        return newsService.queryAllNews(newsVo);
    }

    @RequestMapping("/downloadShowFile")
    public ResponseEntity<Object> downloadShowFile(String path, HttpServletResponse response) {
        return AppFileUtils.downloadFile(response, path, "");
    }

    @RequestMapping("/queryNewId")
    public News queryNewById(NewsVo newsVo){
        return newsService.queryNewById(newsVo);
    }
}
